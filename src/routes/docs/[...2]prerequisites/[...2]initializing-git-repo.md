---
title: Initializing Git Repo
description: Deploying to AWS.
---

# {$frontmatter.title}

{$frontmatter.description}

## Prerequisites

- Make sure you have a [Jamstack App]([...1]creating-a-jamstack-app.md) ready to host
- A [Github Account](https://github.com/join)
- [Git](https://github.com/git-guides/install-git) installed on your system

### Initializing git and push the application to the new GitHub repo

:::steps

!!!step title="Step 1"|description="Run the init cmd to initialize the repo."|orientation="vertical"

```bash copy
git init
```

!!!

!!!step title="Step 2"|description="`git add .` will add all files to git repo to track."|orientation="vertical"

```bash copy
git add .
```

!!!

!!!step title="Step 3"|description="Commit your files to git."|orientation="vertical"

```bash copy
git commit -m “initial commit”
```

!!!

!!!step title="Step 4"|description="It will rename the branch to main."|orientation="vertical"

```bash copy
git branch -M main
```

!!!

!!!step title="Step 5"|description="Replace `username` and `reponame` with yours."|orientation="vertical"

```bash copy
git remote add origin git@github.com:git@github.com:username/reponame.git
```

!!!

!!!step title="Step 6"|description="Push repo to github."|orientation="vertical"

```bash copy
git push origin main
```

!!!
:::