---
title: AWS Amplify
description: Deploying to AWS Amplify.
---

# {$frontmatter.title}

{$frontmatter.description}

## Prerequisites

- A [Jamstack App](../[...2]prerequisites/[...1]creating-a-jamstack-app.md) ready to be deployed.
- Initialized [Github Repo](../[...2]prerequisites/[...2]initializing-git-repo.md)
- An AWS account you can create one at [aws.amazon.com](https://aws.amazon.com)

## Features

:::yes
CI/CD
:::

:::yes
Custom Domain
:::

:::yes
CI/CD
:::

:::yes
SSL
:::

:::yes
CLI Support
:::

## Getting started

:::steps

!!!step title="Go to AWS Amplify Dashboard"|description="Login and click on New App and select Host Web App."|orientation="vertical"

<img src="/assets/aws-amplify/get-started.png" alt="AWS-Amplify Dashboard" width="100%" height="auto">

!!!

!!!step title="Select Git Provider"|description="Now select Github and authorize it."|orientation="vertical"

<img src="/assets/aws-amplify/select-git-provider.png" alt="Select git repo" width="100%" height="auto">

!!!

!!!step title="Select Git Repo"|description="From the list select your app repo."|orientation="vertical"

<img src="/assets/aws-amplify/select-git-repo.png" alt="Select git repo" width="100%" height="auto">

!!!

!!!step title="Verify App & Build Settings"|description="Customize App & Build Settings if needed"|orientation="vertical"

<img src="/assets/aws-amplify/select-git-repo.png" alt="Select git repo" width="100%" height="auto">

!!!

!!!step title="Advance Settings"|description="You can change the docker image for build container and add env values."|orientation="vertical"

<img src="/assets/aws-amplify/advance-settings.png" alt="Select git repo" width="100%" height="auto">

!!!

!!!step title="Review your Configuration"|description="After reviewing your configuration hit save and deploy."|orientation="vertical"

<img src="/assets/aws-amplify/review-configuration.png" alt="Select git repo" width="100%" height="auto">

!!!
:::

> Congratulations :tada:, you have successfully deployed your site to {$frontmatter.title}.
